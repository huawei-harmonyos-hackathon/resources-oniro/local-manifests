# Local Manifests
Repo local manifests for HarmonyOS Hackathon resources

## Dependencies
The manifest brings Hackathon resources meta layers on top of Oniro release.    
The targeted flavour for low resources devices is Zephyr, so the build host should install dependencies for Oniro and Zephyr.   
The targeted flavour for medium-high resources devices is Linux so the build host should install dependencies for Oniro and Linux.   

### Meta DPE Low Dependencies
1. Install repo tool: https://docs.oniroproject.org/en/latest/oniro/repo-workspace.html    
2. Install Zephyr build dependencies with latest SDK (if targeting Low devices): https://docs.zephyrproject.org/latest/getting_started/index.html
3. Install Oniro build dependencies: https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html#prerequisites

### Meta DPE High Dependencies
1. Install repo tool: https://docs.oniroproject.org/en/latest/oniro/repo-workspace.html    
3. Install Oniro build dependencies: https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html#prerequisites

## Integrate
The manifest should be applied on top of Oniro.
1. Clone Oniro manifest    
```
# repo init -u https://booting.oniroproject.org/distro/oniro
# repo sync --no-clone-bundle
```
2. Clone this repository as repo local manifest
```
# git clone git@gitlab.com:harmonyos-hackathon/resources-oniro/local-manifests.git .repo/local_manifests -b main
# repo sync --no-clone-bundle
```
3. Follow the meta layer instructions for building DPE targets   
DPE High Devices: https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-high     
DPE Low Devices: https://gitlab.com/harmonyos-hackathon/resources-oniro/meta-dpe-low    

